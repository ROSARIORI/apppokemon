package com.example.apppokemon.model

import com.google.gson.annotations.SerializedName

class PokemonResponse {
    @SerializedName("results")
    var results: ArrayList<Pokemon>? = null
}